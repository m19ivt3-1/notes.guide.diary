﻿using System;
using System.Collections.Generic;
using GRPC_XamarinClient.ViewModels;
using GRPC_XamarinClient.Views;
using Xamarin.Forms;

namespace GRPC_XamarinClient
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
