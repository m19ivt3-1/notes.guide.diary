﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using GRPC_XamarinClient.Models;
using GRPC_XamarinClient.Views;
using GRPC_XamarinClient.ViewModels;

namespace GRPC_XamarinClient.Views
{
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel _viewModel;

        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new ItemsViewModel(this);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }

        private void Button_Clicked(object sender, EventArgs e) => _viewModel.DeleteItemCommand.Execute(((Button)sender).CommandParameter);
    }
}