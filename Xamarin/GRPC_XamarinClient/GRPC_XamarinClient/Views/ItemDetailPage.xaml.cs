﻿using System.ComponentModel;
using Xamarin.Forms;
using GRPC_XamarinClient.ViewModels;

namespace GRPC_XamarinClient.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel(this);
        }
    }
}