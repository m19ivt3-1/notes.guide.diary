﻿using GRPC;
using GRPC_XamarinClient.Services.SignalR;
using System.Threading.Tasks;

namespace GRPC_XamarinClient.Services
{
    public class SessionSet
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Endpoint { get; set; }
        public string Name { get; set; }

        public Login getLoginData() => new Login() { Login_ = Login, Password = Password };
        public Login getRegistrationData() => new Login { Login_ = Login, Password = Password, AdditionalData = $"newUser|{Name}" };

        public string GUID { get; set; }

        public ServiceClient AuthServiceClient { get; private set; }
        public ServiceClient DiaryServiceClient { get; private set; }

        public async Task<bool> StartSession(string login, string passwd, string endpoint, string name = null) 
        {
            StopSession();
            Login = login;
            Password = passwd;
            Endpoint = endpoint;

            AuthServiceClient = new ServiceClient(Endpoint, "/AuthService");
            DiaryServiceClient = new ServiceClient(Endpoint, "/DiaryService");
            if (!await AuthServiceClient.Start())
                return false;
            if (!await DiaryServiceClient.Start())
                return false;
            return true;
        }

        public async void StopSession() 
        {
            if (AuthServiceClient != null && !AuthServiceClient.IsStopped && AuthServiceClient.IsConnected)
                await AuthServiceClient.Stop();
            if (DiaryServiceClient != null && !DiaryServiceClient.IsStopped && DiaryServiceClient.IsConnected)
                await DiaryServiceClient.Stop();
        }
    }
}