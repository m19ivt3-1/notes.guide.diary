﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GRPC;
using GRPC_XamarinClient.Models;
using Xamarin.Forms;
using Microsoft.AspNetCore.SignalR.Client;

namespace GRPC_XamarinClient.Services
{
    public class NGDDataStore : IDataStore<Note> 
    {
        private List<Note> items;
        private readonly SessionSet session;

        public Page owner;

        public NGDDataStore() 
        {
            session = DependencyService.Get<SessionSet>();
            items = new List<Note>();
        }

        public async Task<bool> AddItemAsync(Note note) 
        {
            var res_note = await session.DiaryServiceClient.HubConnection.InvokeAsync<Note>("setNote", note);
            if (res_note.GUID == $"Error_{ Guid.Empty}")
            {
                if (owner != null)
                    await owner.DisplayAlert("Error", res_note.Content, "OK");
                return false;
            }
            var e_note = items.FirstOrDefault(n => n.GUID == note.GUID && n.Name == note.Name);
            if (e_note == null)
            {
                items.Add(e_note);
            }
            return true;
        }

        public async Task<bool> UpdateItemAsync(Note note) => await AddItemAsync(note);

        public async Task<bool> DeleteItemAsync(string id) 
        {
            var e_note = items.FirstOrDefault(n => n.GUID == session.GUID && n.Name == id);
            if (e_note == null)
                return true;
            var error = await session.DiaryServiceClient.HubConnection.InvokeAsync<Error>("deleteNote", e_note);
            if (error.IsError)
                await owner?.DisplayAlert("Ошибка", error.Error_, "OK");
            else
                items.Remove(e_note);
            return !error.IsError;
        }

        public async Task<Note> GetItemAsync(string id) => await session.DiaryServiceClient.HubConnection.InvokeAsync<Note>("getNote", new Note { GUID = session.GUID, Name = id });

        public async Task<IEnumerable<Note>> GetItemsAsync(bool forceRefresh = false)
        {
            var noteList = await session.DiaryServiceClient.HubConnection.InvokeAsync<string[]>("getNoteNames", new Session() { GUID = session.GUID });
            items.Clear();
            if (noteList.Length > 0) 
            {
                if (noteList[0] == $"{session.GUID}_ERROR") 
                {
                    owner?.DisplayAlert("ОШИБКА", "Не удалось получить список записей", "OK");
                }
                for(int i = 0; i < noteList.Length; i++) 
                {
                    items.Add(new Note { GUID = session.GUID, Name = noteList[i] });
                }
            }
            return items;
        }
    }

    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "First item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Second item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Third item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Fourth item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Fifth item", Description="This is an item description." },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Sixth item", Description="This is an item description." }
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id) => await Task.FromResult(items.FirstOrDefault(s => s.Id == id));

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false) => await Task.FromResult(items);
    }
}