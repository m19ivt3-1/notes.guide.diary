﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using GRPC_XamarinClient.Models;
using GRPC_XamarinClient.Views;
using GRPC;
using GRPC_XamarinClient.Services;
using System.Linq;

namespace GRPC_XamarinClient.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        private Note _selectedItem;

        public ObservableCollection<Note> Items { get; }
        public Command LoadItemsCommand { get; }
        public Command AddItemCommand { get; }
        public Command<Note> ItemTapped { get; }

        public Command DeleteItemCommand { get; }

        private NGDDataStore dataStore;
        private readonly ItemsPage owner;

        public ItemsViewModel()
        {
            Title = "Notes";
            Items = new ObservableCollection<Note>();
            dataStore = DependencyService.Get<NGDDataStore>();
            dataStore.owner = owner;

            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            ItemTapped = new Command<Note>(OnItemSelected);

            AddItemCommand = new Command(OnAddItem);

            DeleteItemCommand = new Command(async (obj) => 
            {
                if (obj == null)
                    return;
                if (await DependencyService.Get<NGDDataStore>().DeleteItemAsync((string)obj))
                {
                    IsBusy = true;
                    for (int i = 0; i < Items.Count; i++)
                    {
                        if (Items[i].Name == (string)obj)
                        {
                            Items.RemoveAt(i);
                            break;
                        }
                    }
                    IsBusy = false;
                }
            });
        }

        public ItemsViewModel(ItemsPage owner) : this() 
        {
            this.owner = owner;
        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await dataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                owner?.DisplayAlert("Ошибка", ex.ToString(), "OK");
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public Note SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }

        private async void OnAddItem(object obj) => await Shell.Current.GoToAsync(nameof(NewItemPage));

        async void OnItemSelected(Note item)
        {
            if (item == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(ItemDetailPage)}?{nameof(ItemDetailViewModel.ItemId)}={item.Name}");
        }
    }
}