﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using GRPC;
using GRPC_XamarinClient.Models;
using GRPC_XamarinClient.Services;
using Xamarin.Forms;

namespace GRPC_XamarinClient.ViewModels
{
    public class NewItemViewModel : BaseViewModel
    {
        private string name;
        private string content;

        public NewItemViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged += (_, __) => SaveCommand.ChangeCanExecute();
        }

        private bool ValidateSave() => !string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(content);

        public string Name 
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string Content 
        {
            get => content;
            set => SetProperty(ref content, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            var session = DependencyService.Get<SessionSet>();
            var storage = DependencyService.Get<NGDDataStore>();
            var note = new Note()
            {
                GUID = session.GUID,
                Name = name,
                Content = content
            };
            await storage.AddItemAsync(note);

            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
    }
}
