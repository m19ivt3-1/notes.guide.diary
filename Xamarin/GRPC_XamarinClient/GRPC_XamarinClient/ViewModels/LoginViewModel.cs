﻿using GRPC;
using GRPC_XamarinClient.Services;
using GRPC_XamarinClient.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Microsoft.AspNetCore.SignalR.Client;
using System.Net;

namespace GRPC_XamarinClient.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public Command LoginCommand { get; }
        public Command RegistrationCommand { get; }
        public Command LoginOpenCommand { get; }

        public string EMail { get; set; }
        public string Password { get; set; }
        public string Password2 { get; set; }
        public string Endpoint { get; set; }

        public string Name { get; set; }
        private void setWindow(string window) 
        {
            IsLoginVibsible = window == "login";
            IsRegistrationVisible = window == "registration";
            OnPropertyChanged(nameof(IsLoginVibsible));
            OnPropertyChanged(nameof(IsRegistrationVisible));
        }
        private string getCurrentWindow() 
        {
            if (IsLoginVibsible)
                return "login";
            else if (IsRegistrationVisible)
                return "registration";
            else
                return "unknown";
        }
        public bool IsLoginVibsible { get; set; } = true;
        public bool IsRegistrationVisible { get; set; } = false;
        public string ErrorText { get; set; }

        private LoginPage owner;

        public LoginViewModel()
        {
            LoginCommand = new Command(OnLoginClicked);
            RegistrationCommand = new Command(OnRegistrationClicked);
            LoginOpenCommand = new Command(OnLoginOpenClicked);
        }

        public LoginViewModel(LoginPage owner) : this()
        {
            this.owner = owner;
            this.Endpoint = "https://192.168.1.2:5002";
        }

        private async void OnLoginClicked(object obj)
        {
            IsBusy = true;
            var isNewUser = obj != null && (string)obj == "NewUser";
            if (isNewUser && Password != Password2) 
            {
                await owner?.DisplayAlert("Ошибка", "ОШИБКА! Пароли не совпадают!", "OK");
                IsBusy = false;
            }
            var settings = DependencyService.Get<SessionSet>();
            var st = await settings.StartSession(EMail, Password, Endpoint, Name);
            try
            {
                var response = await settings.AuthServiceClient.HubConnection.InvokeAsync<Session>("login", isNewUser ? settings.getRegistrationData() : settings.getLoginData());
                if (!response.Error.IsError)
                {
                    settings.GUID = response.GUID;
                    await Shell.Current.GoToAsync($"//{nameof(ItemsPage)}");
                }
                else
                {
                    await owner?.DisplayAlert("Ошибка", "ОШИБКА! " + response.Error.Error_, "OK");
                }
                IsBusy = false;
            }
            catch(Exception ex)
            {
                IsBusy = false;
                await owner?.DisplayAlert("Ошибка", "ОШИБКА! Не удалось вызвать метод 'login'", "OK");
            }
        }
        private void OnRegistrationClicked(object obj) => setWindow("registration");
        private void OnLoginOpenClicked(object obj) => setWindow("login");
    }
}
