﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using GRPC_XamarinClient.Models;
using GRPC_XamarinClient.Services;
using GRPC_XamarinClient.Views;
using Xamarin.Forms;

namespace GRPC_XamarinClient.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class ItemDetailViewModel : BaseViewModel
    {
        private string guid;
        private string name;
        private string content;

        private ItemDetailPage owner;

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string Content
        {
            get => content;
            set => SetProperty(ref content, value);
        }

        public string ItemId
        {
            get => name;
            set
            {
                name = value;
                LoadItemId(value);
            }
        }

        public async void LoadItemId(string itemId)
        {
            try
            {
                itemId = System.Web.HttpUtility.UrlDecode(itemId);
                var item = await DependencyService.Get<NGDDataStore>().GetItemAsync(itemId);
                guid = item.GUID;
                Name = item.Name;
                OnPropertyChanged(nameof(Name));
                Content = item.Content;
            }
            catch (Exception)
            {
                owner?.DisplayAlert("Ошибка", "Failed to Load Item", "OK");
            }
        }

        public Command SaveCommand { get; set; }
        public Command BackCommand { get; set; }

        public ItemDetailViewModel(ItemDetailPage owner)
        {
            this.owner = owner;
            var storage = DependencyService.Get<NGDDataStore>();
            storage.owner = owner;
            SaveCommand = new Command(async obj => 
            {
                await storage.UpdateItemAsync(new GRPC.Note { GUID = guid, Name = name, Content = content });
                await Shell.Current.GoToAsync($"//{nameof(ItemsPage)}");
            });
            BackCommand = new Command(async obj => 
            {
                await Shell.Current.GoToAsync($"//{nameof(ItemsPage)}");
            });
        }
    }
}
