﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace GRPC_XamarinClient.Services.SignalR 
{
    public class ServiceClient 
    {
        public HubConnection HubConnection { get; private set; }
        public bool IsConnected { get; private set; } = false;
        public bool IsStopped { get; private set; } = false;

        public ServiceClient(string endpoint_address, string hubPath)
        {
            HubConnection = new HubConnectionBuilder().WithUrl($"{endpoint_address}{hubPath}", options => 
            {
                //https://stackoverflow.com/questions/61905509/signalr-the-ssl-connection-could-not-be-established
                options.WebSocketConfiguration = conf =>
                {
                    conf.RemoteCertificateValidationCallback = (message, cert, chain, errors) => { return true; };
                };
                options.HttpMessageHandlerFactory = factory => new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
                };
                //options.AccessTokenProvider = () => Task.FromResult(Token);
            }).Build();
            HubConnection.Closed += async (error) =>
            {
                if (!IsStopped)
                    await Start();
            };
        }

        public async Task<bool> Start() 
        {
            try
            {
                await HubConnection.StartAsync();
                IsConnected = true;
                return true;
            }
            catch
            {
                IsConnected = false;
                return false;
            }
        }

        public async Task Stop() 
        {
            IsStopped = true;
            try 
            {
                await HubConnection.StopAsync();
            }
            catch 
            {
            }
        }
    }
}