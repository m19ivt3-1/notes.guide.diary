﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GRPC_XamarinClient.Services;
using GRPC_XamarinClient.Views;
using System.Net.Http;
using GRPC;
using Grpc.Core;
using Microsoft.AspNetCore.SignalR.Client;

namespace GRPC_XamarinClient
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            DependencyService.Register<SessionSet>();
            DependencyService.Register<NGDDataStore>();
            MainPage = new AppShell();
        }

        protected /*async*/ override void OnStart()
        {
            //клиент к SignalR (как рабочий пример)
            /*HubConnection hubConnection = new HubConnectionBuilder().WithUrl("http://192.168.1.2:5000/AuthService").Build();
            hubConnection.Closed += async (error) =>
            {
                var t = "Closed";
            };
            hubConnection.On<Session>("onLogin", (resp) => 
            {
                System.Diagnostics.Debug.WriteLine($"OonLogin, {resp}");
            });
            try 
            {
                await hubConnection.StartAsync();
            }
            catch(Exception ex) 
            {

            }
            try
            {
                await hubConnection.InvokeAsync("login", new Login() { Login_ = "test", Password = "test" });
            }
            catch (Exception ex)
            {
            }*/
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
