using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Backend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        // Additional configuration is required to successfully run gRPC on macOS.
        // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder =>
            {
                //webBuilder.UseUrls("http://*:5000", "https://*:5001");
                webBuilder.ConfigureKestrel(KestrelOptions =>
                {
                    KestrelOptions.ListenAnyIP(5000, cconf => cconf.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http1);
                    KestrelOptions.ListenAnyIP(5001, cconf => cconf.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2);
                    KestrelOptions.ListenAnyIP(5002, cconf =>
                    {
                        cconf.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http1;
                        cconf.UseHttps("Keys/M19IVT3-1.pfx", "m19ivt3120201mag");
                    });
                    KestrelOptions.ListenAnyIP(5003, cconf =>
                    {
                        cconf.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2;
                        cconf.UseHttps("Keys/M19IVT3-1.pfx", "m19ivt3120201mag");
                    });
                });
                webBuilder.UseStartup<Startup>();
            });
        //���� ������ �� ����: KARLYSHEV, �������� ����������: M19IVT3-1
        //cd C:\Program Files\Git\usr\bin
        //������� ����������:
        //openssl req -newkey rsa:2048 -nodes -keyout KARLYSHEV.key -out KARLYSHEV.csr
        //RU/Russia/Nizhny Novgorod/NNGTU/M19IVT3-1/M19IVT3-1
        //m19ivt3120201mag
        //
        //����������� ���������� (���������������):
        //openssl x509 -signkey KARLYSHEV.key -in KARLYSHEV.csr -req -days 365 -out M19IVT3-1.crt
        //������������ � pfx: 
        //openssl pkcs12 -export -out M19IVT3-1.pfx -inkey KARLYSHEV.key -in M19IVT3-1.crt
        //������ ��� ������: m19ivt3120201mag
    }
}
