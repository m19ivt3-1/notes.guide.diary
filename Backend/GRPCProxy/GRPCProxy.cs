﻿namespace GRPC 
{
    public static class GRPCNative
    {
        private static Impl.AuthService authService;
        private static Impl.DiaryService diaryService;

        public static Impl.AuthService AuthService => authService ?? (authService = new Impl.AuthService());
        public static Impl.DiaryService DiaryService => diaryService ?? (diaryService = new Impl.DiaryService());
    }

}