﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

//Moved from GRPCServer project
namespace GRPC.Impl 
{
    public class DiaryService : GRPC.DiaryService.DiaryServiceBase 
    {
        private DB_EFCore.DbController DbController => new DB_EFCore.DbController();

        private DB_EFCore.Note ToDBNote(Note grpc_note) 
        {
            var result = new DB_EFCore.Note();
            result.Content = grpc_note.Content;
            result.CreatedDate = grpc_note.CreatedDate != null ? grpc_note.CreatedDate.ToDateTime(): DateTime.UtcNow;
            result.Id = $"{grpc_note.GUID}_{grpc_note.Name}";
            result.Name = grpc_note.Name;
            result.UpdatedDate = grpc_note.UpdatedDate != null ? grpc_note.UpdatedDate.ToDateTime() : DateTime.UtcNow;
            result.UserId = grpc_note.GUID;
            return result;
        }

        public static Note ToGRPCNote(DB_EFCore.Note db_note) 
        {
            var result = new Note();
            result.Content = db_note.Content;
            result.CreatedDate = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTime(db_note.CreatedDate.ToUniversalTime());
            result.GUID = db_note.UserId;
            result.Name = db_note.Name;
            result.UpdatedDate = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTime(db_note.UpdatedDate.ToUniversalTime());
            return result;
        }

        public async override Task<NoteList> getNoteList(Session request, ServerCallContext context)
        {
            try 
            {
                NoteList outList = new NoteList();
                List<DB_EFCore.Note> list = await DbController.GetNoteList(request.GUID);
                outList.GUID = request.GUID;
                if (list != null && list.Count > 0)
                    outList.Names.Add(list.Select(n => n.Name));
                return outList;
            }
            catch(Exception ex) 
            {
                return new NoteList() { GUID = Guid.Empty.ToString() };
            }
        }

        public async Task<string[]> getNoteNames(Session request) 
        {
            try 
            {
                List<DB_EFCore.Note> list = await DbController.GetNoteList(request.GUID);
                return list.Select(n => n.Name).ToArray();
            }
            catch (Exception ex) 
            {
                return new string[] { request.GUID + "_ERROR", ex.Message };
            }
        }

        public async override Task<Error> deleteNoteList(NoteList request, ServerCallContext context)
        {
            try 
            {
                bool isDeleted = await DbController.DeleteNoteList(request.GUID, request.Names.ToList());
                return new Error { IsError = !isDeleted };
            }
            catch (Exception ex) 
            {
                return new Error() { IsError = true, Error_ = ex.Message };
            }
        }

        public async override Task<Note> getNote(Note request, ServerCallContext context)
        {
            try 
            {
                DB_EFCore.Note note = new DB_EFCore.Note();
                note.UserId = request.GUID;
                note.Name = request.Name;
                note = await DbController.GetNote(note);
                return ToGRPCNote(note);
            }
            catch (Exception ex) 
            {
                return new Note() { GUID = $"Error_{ Guid.Empty}", Content = ex.Message };
            }
        }

        public async override Task<Note> setNote(Note request, ServerCallContext context)
        {
            try 
            {
                DB_EFCore.Note note = await DbController.SetNote(ToDBNote(request));
                return ToGRPCNote(note);
            }
            catch (Exception ex) 
            {
                return new Note() { GUID = $"Error_{ Guid.Empty}", Content = ex.Message };
            }
        }

        public async override Task<Error> deleteNote(Note request, ServerCallContext context)
        {
            try 
            {
                bool isDeleted = await DbController.DeleteNote(ToDBNote(request));
                return new Error { IsError = !isDeleted };
            }
            catch (Exception ex) 
            {
                return new Error { IsError = true, Error_ = ex.Message };
            }
        }
    }
}