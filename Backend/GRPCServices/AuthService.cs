﻿using Grpc.Core;
using System;
using System.Threading.Tasks;

//Moved from GRPCServer project
namespace GRPC.Impl 
{
    public class AuthService : GRPC.AuthService.AuthServiceBase
    {
        public async override Task<Session> login(Login request, ServerCallContext context)
        {
            if (request == null)
                return new Session { GUID = Guid.Empty.ToString(), Error = new Error { IsError = true, Error_ = "Нет данных" } };
            if (request.AdditionalData != null && request.AdditionalData.StartsWith("newUser")) 
            {
                var dbContr = new DB_EFCore.DbController();
                var regData = request.AdditionalData.Split('|');
                if (regData.Length < 2)
                    return new Session { GUID = Guid.Empty.ToString(), Error = new Error { IsError = true, Error_ = "Не указано имя пользователя" } };
                try
                {
                    bool isCreated = await dbContr.CreateUser(new DB_EFCore.User() { Name = regData[1], email = request.Login_, PasswordHash = request.Password });
                }
                catch (Exception ex)
                {
                    return new Session { GUID = Guid.Empty.ToString(), Error = new Error { IsError = true, Error_ = ex.Message } };
                }
            }
            try
            {
                var dbContr = new DB_EFCore.DbController();
                string userId = await dbContr.GetUserGUID(request.Login_, request.Password);
                if (userId == null)
                    return new Session { GUID = Guid.Empty.ToString(), Error = new Error { IsError = true, Error_ = "Ошибка при запросе к серверу базы данных" } };
                else
                    return new Session { GUID = userId, Error = new Error { IsError = false } };
            }
            catch (Exception ex)
            {
                return new Session { GUID = Guid.Empty.ToString(), Error = new Error { IsError = true, Error_ = ex.Message } };
            }
        }

        public override Task<Logout> logout(Session request, ServerCallContext context)
        {
            throw new NotImplementedException();
           // return Task.FromResult(new Logout { Login = "User", Error = null });
        }
    }
}