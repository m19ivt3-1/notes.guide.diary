﻿using GRPC;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalRServices
{
    public class AuthService : Hub
    {
        public async Task<Session> login(Login request) => await GRPCNative.AuthService.login(request, null);
    }
}