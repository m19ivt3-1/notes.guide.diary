﻿using GRPC;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace SignalRServices
{
    public class DiaryService : Hub
    {
        public async Task<Note> getNote(Note request) => await GRPCNative.DiaryService.getNote(request, null);

        public async Task<NoteList> getNoteList(Session request) => await GRPCNative.DiaryService.getNoteList(request, null);

        public async Task<string[]> getNoteNames(Session request) => await GRPCNative.DiaryService.getNoteNames(request);


        public async Task<Note> setNote(Note request) => await GRPCNative.DiaryService.setNote(request, null);

        public async Task<Error> deleteNote(Note request) => await GRPCNative.DiaryService.deleteNote(request, null);

        public async Task<Error> deleteNoteList(NoteList request) => await GRPCNative.DiaryService.deleteNoteList(request, null);
    }
}