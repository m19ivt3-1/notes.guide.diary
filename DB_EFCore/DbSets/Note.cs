﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DB_EFCore 
{
    public class Note 
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }
    }
}