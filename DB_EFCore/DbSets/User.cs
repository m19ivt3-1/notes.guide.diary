﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DB_EFCore 
{
    public class User 
    {
        [Key]
        public string Id { get; set; }
        public string email { get; set; } //в качестве логина
        public string Name { get; set; }
        public string PasswordHash { get; set; }
        
        public List<Note> Notes { get; set; }
    }
}