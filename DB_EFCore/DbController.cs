﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DB_EFCore 
{
    public class DbController 
    {
        private DiaryDatabaseContext GetDbContext() => new DiaryDatabaseContext("server=localhost;port=3306;UserId=workUser;password=RV2wICXg;database=DiaryDB");

        public async Task<bool> CreateUser(User user) 
        {
            return await Task.Run(() =>
            {
                if (user == null || user.email == null || user.Name == null || user.PasswordHash == null || user.PasswordHash.Length < 6)
                    throw new Exception("Данные некорректны");
                var isEmailFree = true;
                DiaryDatabaseContext context = null;
                try
                {
                    context = GetDbContext();
                    if (context.Users.AsNoTracking().Any(user_ => user_.email == user.email))
                        isEmailFree = false;
                }
                catch 
                {
                    throw new Exception("Ошибка при запросе к серверу базы данных");
                }
                if(!isEmailFree)
                    throw new Exception("email занят");
                try
                {
                    while (true)
                    {
                        var newGUID = Guid.NewGuid().ToString();
                        if (context.Users.Find(newGUID) == null)
                        {
                            user.Id = newGUID;
                            user.PasswordHash = Other.Security.HashPassword(user.PasswordHash);
                            context.Users.Add(user);
                            context.SaveChanges();
                            return true;
                        }
                    }
                }
                catch
                {
                    return false;
                }
            });
        }

        public async Task<string> GetUserGUID(string email, string password) 
        {
            return await Task.Run(() =>
            {
                if (email == null || password == null)
                    throw new Exception("Данные некорректны");
                User user = null;
                try
                {
                    var context = GetDbContext();
                    user = context.Users.AsNoTracking().FirstOrDefault(u => u.email == email);
                }
                catch (Exception ex)
                {
                    return null;
                }
                if (user == null)
                    throw new Exception("Пользователь не существует");
                if (!Other.Security.VerifyHashedPassword(user.PasswordHash, password))
                    throw new Exception("Неправильный пароль");
                return user.Id;
            });
        }

        public async Task<List<Note>> GetNoteList(string userGUID)
        {
            return await Task.Run(() =>
            {
                try
                {
                    return GetDbContext().Notes.Where(note => note.UserId == userGUID).ToList();
                }
                catch
                {
                    throw new Exception("Ошибка при получении списка");
                }
            });
        }

        public async Task<bool> DeleteNoteList(string userGUID, List<string> notes) 
        {
            return await Task.Run(() =>
            {
                if (notes.Count == 0)
                    return true;
                try
                {
                    var context = GetDbContext();
                    var noteList = context.Notes.Where(note => note.UserId == userGUID).ToList(); //получаем список записок
                    for (int i = 0; i < noteList.Count; i++)
                    {
                        if (notes.Contains(noteList[i].Name))
                        {
                            context.Entry(noteList[i]).State = EntityState.Deleted;
                        }
                    }
                    context.SaveChanges();
                    return true;
                }
                catch
                {
                    throw new Exception("Ошибка при добавлении пользователя");
                }
            });
        }

        public async Task<Note> GetNote(Note note) 
        {
            return await Task.Run(() => 
            {
                if (note == null || note.UserId == null || note.UserId.Length == 0 || note.Name == null || note.Name.Length == 0)
                    return null;
                try
                {
                    //var tr = System.Text.Encoding.UTF8.
                    return GetDbContext().Notes.Find($"{note.UserId}_{note.Name}");
                }
                catch
                {
                    throw new Exception("Ошибка получения записи");
                }
            });
        }

        public async Task<Note> SetNote(Note note)
        {
            return await Task.Run(() =>
            {
                if (note == null || note.UserId == null || note.UserId.Length == 0 || note.Name == null || note.Name.Length == 0)
                    return null;
                try
                {
                    var context = GetDbContext();
                    var noteDb = context.Notes.Find($"{note.UserId}_{note.Name}");
                    if (noteDb != null) //edit
                    {
                        noteDb.Content = note.Content;
                        noteDb.UpdatedDate = DateTime.UtcNow;
                        context.Entry(noteDb).State = EntityState.Modified;
                        note = noteDb;
                    }
                    else //add
                    {
                        note.Id = $"{note.UserId}_{note.Name}";
                        note.CreatedDate = DateTime.UtcNow;
                        note.UpdatedDate = DateTime.UtcNow;
                        context.Notes.Add(note);
                    }
                    context.SaveChanges();
                    return note;
                }
                catch
                {
                    throw new Exception("Ошибка сохранения записи");
                }
            });
        }

        public async Task<bool> DeleteNote(Note note)
        {
            return await Task.Run(() =>
            {
                if (note == null || note.UserId == null || note.UserId.Length == 0 || note.Name == null || note.Name.Length == 0)
                    return false;
                try
                {
                    var context = GetDbContext();
                    var notedb = context.Notes.Find($"{note.UserId}_{note.Name}");
                    if (notedb != null) 
                    {
                        context.Entry(notedb).State = EntityState.Deleted;
                        context.SaveChanges();
                    }
                    return true;
                }
                catch
                {
                    throw new Exception("Ошибка удаления записи");
                }
            });
        }
    }
}