﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DB_EFCore 
{
    public class DiaryDatabaseContext : DbContext 
    {
        private string dbConnectionString;

        public DbSet<Note> Notes { get; set; }
        public DbSet<User> Users { get; set; }

        public DiaryDatabaseContext(string dbconnection)
        {
            this.dbConnectionString = dbconnection;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            optionsBuilder.UseMySql(dbConnectionString);
        }
    }
}